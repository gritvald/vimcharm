Installation
============
wget -q https://raw.githubusercontent.com/korzeniewskipl/vimcharm/master/install.sh -O- | sh


Features
========
* use of the mouse
* cursor line
* line numbers
* highlight search
* incremental search
* smart case search
* highlighting of trailing spaces
* right margin line
* easier moving of code blocks - just select several lines of code and then press '<' or '>' several times
* accessing the system clipboard

For more features provided by the included plugins, please see the documentation for each of them.


Color Schemes
=============
* `Apprentice <https://github.com/romainl/Apprentice>`_
* `hybrid.vim <https://github.com/w0ng/vim-hybrid>`_
* `solarized <https://github.com/altercation/vim-colors-solarized>`_
* `wombat256mod <https://github.com/vim-scripts/wombat256.vim>`_


Keymap
======
* <F2> - Quick write session.
* <F3> - Quick load session.
* <F4> - Toggle paste mode.
* <F8> - Run Tagbar.
* <C-p> - Run CtrlP.
* ,t - Run NERD Tree.
* ,b - Set/unset breakpoint.

Text
----
* ,cc - Comment selected code.
* ,cu - Uncomment selected code.
* ,r - Reformat all/selected paragraph lines.

Tagbar
------
* <Enter> - Jump to the tag.
* <C-n> - Next top-level tag.
* <C-p> - Prev top-level tag.
* p - Jump to the tag, but stay in the Tagbar window.
* s - Toggle sort order between name and file order.
* o - Toggle the fold.
* * - Open all folds.
* = - Close all folds.

Windows
-------
* <C-j> - Move cursor to window below the current one.
* <C-k> - Move cursor to window above the current one.
* <C-l> - Move cursor to window right of the current one.
* <C-h> - Move cursor to window left of the current one.
* <C-w>n - Creates a new window above the current one.
* <C-w>s - Splits the current window -- creates a new window editing the same file as the current window.
* <C-w>o - Make the current window the only window (close all other windows).

Tabs
----
* ,n - Previous tab.
* ,m - Next tab.


Plugins
=======
* `pathogen.vim <https://github.com/tpope/vim-pathogen>`_
* `powerline <https://github.com/powerline/powerline>`_
* `CtrlP <https://github.com/kien/ctrlp.vim>`_
* `NERD Commenter <https://github.com/scrooloose/nerdcommenter>`_
* `NERD Tree <https://github.com/scrooloose/nerdtree>`_
* `Python-mode <https://github.com/klen/python-mode>`_
* `vim-ansible-yaml <https://github.com/chase/vim-ansible-yaml>`_
* `vim-gitgutter <https://github.com/airblade/vim-gitgutter>`_
* `vim-fugitive <https://github.com/tpope/vim-fugitive>`_
* `Riv <https://github.com/Rykka/riv.vim>`_
* `Tagbar <https://github.com/majutsushi/tagbar>`_
* `jedi-vim <https://github.com/davidhalter/jedi-vim>`_
* `supertab <https://github.com/ervandew/supertab.git>`_


Links
=====
* `Beautiful Vim Cheat-Sheet Poster <http://vimcheatsheet.com/>`_
* `My vi/vim cheatsheet <http://www.worldtimzone.com/res/vi.html>`_
* `vimpdb <https://pypi.python.org/pypi/vimpdb/>`_

* `9 Enhancements to Shell and Vim Productivity <http://danielmiessler.com/blog/enhancements-to-shell-and-vim-productivity/>`_
* `What is the recommended way to use Vim folding for Python code <http://stackoverflow.com/questions/357785/what-is-the-recommended-way-to-use-vim-folding-for-python-code>`_

* `hynek/vim-python-pep8-indent <https://github.com/hynek/vim-python-pep8-indent>`_
* `kien/tabman.vim <https://github.com/kien/tabman.vim>`_
* `onjin/vim-startup <https://github.com/onjin/vim-startup>`_
* `tomislater/dotfiles <https://github.com/tomislater/dotfiles>`_
* `spf13/spf13-vim <https://github.com/spf13/spf13-vim>`_
* `yodiaditya/vim-pydjango <https://github.com/yodiaditya/vim-pydjango>`_

* `snipMate - TextMate-style snippets for Vim <http://www.vim.org/scripts/script.php?script_id=2540>`_
* `python.vim - Enhanced version of the python syntax highlighting script <http://www.vim.org/scripts/script.php?script_id=790>`_
* `pythoncomplete - Python Omni Completion <http://www.vim.org/scripts/script.php?script_id=1542>`_

* `Material theme for Vim <https://github.com/jdkanani/vim-material-theme>`_
* `PaperColor Theme <https://github.com/NLKNguyen/papercolor-theme>`_

* http://dougblack.io/words/a-good-vimrc.html
* https://github.com/nihilifer/vimrc/blob/master/.vimrc
* https://github.com/VundleVim/Vundle.vim
* https://github.com/Valloric/YouCompleteMe
* http://spf13.com/post/the-15-best-vim-plugins
* http://www.commandlinefu.com/commands/view/1204/save-a-file-you-edited-in-vim-without-the-needed-permissions
* `Vim Plugins List <https://www.youtube.com/watch?v=8XGueeQJsrA&feature=youtu.be&t=2m17s>`_
* `Ben Orenstein - Write code faster: expert-level vim (Railsberry 2012) <https://www.youtube.com/watch?v=SkdrYWhh-8s>`_
* https://github.com/tacahiroy/ctrlp-funky
* https://github.com/xolox/vim-easytags
* https://github.com/mhinz/vim-startify
* https://github.com/fisadev/vim-isort
* https://github.com/mkitt/tabline.vim

* `Vim <http://www.vim.org/>`_
* `Vim Awesome <http://vimawesome.com/>`_ - Awesome Vim plugins
* `Vim Bits <http://www.vimbits.com/>`_ - Snippets of a .vimrc
* `Vim Casts <http://vimcasts.org/>`_ - Screencasts about the Vim
* `Vim Color Schemes <https://github.com/rafi/awesome-vim-colorschemes>`_ - Awesome Vim Color Schemes

AutoClose
---------
* http://townk.github.io/vim-autoclose/
* https://github.com/tpope/vim-surround
* https://github.com/Raimondi/delimitMate
* https://github.com/jiangmiao/auto-pairs

Jedi
----
* http://jedi.jedidjah.ch/en/latest/
* http://stackoverflow.com/questions/18279658/jedi-vim-doesnt-work-correctly
* http://unlogic.co.uk/2013/02/08/vim-as-a-python-ide/
* http://vi.stackexchange.com/questions/2403/vim-code-completion-for-python-3
* http://thibaut.horel.org/blog/using-vim-as-a-python-ide.html
* http://packages.ubuntu.com/pl/trusty/vim-python-jedi

Books
-----
* `Edytory vi i Vim. Leksykon kieszonkowy <http://helion.pl/ksiazki/edytory-vi-i-vim-leksykon-kieszonkowy-arnold-robbins,evivim.htm>`_
* `Use Vim Like A Pro <https://leanpub.com/VimLikeAPro>`_
