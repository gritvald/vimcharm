filetype off

call pathogen#infect()
call pathogen#helptags()

filetype plugin indent on
syntax on


" ============================================================================
" Settings
" ============================================================================
set bs=indent,eol,start
set clipboard=unnamedplus
set mouse=a
set cursorline
set modeline
set nowrap
set number


" ============================================================================
" Enable highlight, incremental and smart case searching.
" ============================================================================
set hlsearch
set incsearch
set ignorecase
set smartcase


" ============================================================================
" Disable backup and swap files.
" ============================================================================
set nobackup
set nowritebackup
set noswapfile


" ============================================================================
" Indentation setup.
" ============================================================================
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set shiftround
set autoindent


" ============================================================================
" Keymapping
" ============================================================================
let mapleader = ","
set pastetoggle=<F4>

" Easier moving between tabs.
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" Easier moving of code blocks.
vnoremap < <gv
vnoremap > >gv

" Easier moving around the windows.
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Easier formatting of paragraphs.
vmap <Leader>r gq
nmap <Leader>r gqap
"nmap <Leader>r gq}  " Reformat from the current line to the end of paragraph

" Quick write (F2) and load (F3) session.
map <F2> :mksession! ~/.vim_session<CR>
map <F3> :source ~/.vim_session<CR>

" r as redo.
nnoremap r <c-r>


" ============================================================================
" Enable highlighting of trailing spaces.
" ----------------------------------------------------------------------------
" MUST be inserted BEFORE the colorscheme command.
" See: http://vim.wikia.com/wiki/Highlight_unwanted_spaces
" ============================================================================
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()


" ============================================================================
" Set right margin line.
" ----------------------------------------------------------------------------
" MUST be inserted BEFORE the colorscheme command.
" ============================================================================
set colorcolumn=79
autocmd ColorScheme * highlight ColorColumn ctermbg=229


" ============================================================================
" Color scheme.
" ============================================================================

"hybrid
"set t_Co=256
"set background=dark
"colorscheme hybrid

"solarized
let g:solarized_termcolors=256
set background=light
colorscheme solarized


" ============================================================================
" Powerline -- https://github.com/Lokaltog/powerline
" ============================================================================
set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
set laststatus=2


" ============================================================================
" CtrlP -- https://github.com/kien/ctrlp.vim
" ============================================================================
if executable('ag')
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_use_caching = 0
endif

let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*build/*
set wildignore+=*dist/*
set wildignore+=*.egg-info/*
set wildignore+=*/coverage/*


" ============================================================================
" NERDTree -- https://github.com/scrooloose/nerdtree.git
" ============================================================================
nmap <leader>t :NERDTree<CR>

" Display hidden files.
let NERDTreeShowHidden=1


" ============================================================================
" Riv -- https://github.com/Rykka/riv.vim
" ============================================================================
let g:riv_disable_folding = 1


" ============================================================================
" Tagbar -- https://github.com/majutsushi/tagbar
" ============================================================================
nmap <F8> :TagbarToggle<CR>


" ============================================================================
" Python-mode -- https://github.com/klen/python-mode
" ============================================================================
"let g:pymode_python = "python3"
let g:pymode_options_max_line_length = 79
let g:pymode_folding = 0
let g:pymode_syntax = 1
let g:pymode_virtualenv = 1

" Code checking
let g:pymode_lint_on_write = 1
let g:pymode_lint_unmodified = 1

" Rope support
let g:pymode_rope = 0  " It has to be disabled when using jedi-vim
let g:pymode_rope_regenerate_on_write = 0
let g:pymode_rope_complete_on_dot = 0
let g:pymode_rope_goto_definition_cmd = "tabe"


" ============================================================================
" jedi-vim -- https://github.com/davidhalter/jedi-vim
" ============================================================================
let g:jedi#popup_on_dot = 0
let g:jedi#use_tabs_not_buffers = 1
let g:jedi#usages_command = ""


" ============================================================================
" Supertab -- https://github.com/ervandew/supertab
" ============================================================================
let g:SuperTabDefaultCompletionType = "context"
